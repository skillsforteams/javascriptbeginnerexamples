document.getElementById("PattyField").innerText = ".. place order ..";
let basePrice = 150 ;
let Burger = {  
    name: "standard burger",  
    patty: "beef", 
    salad: "iceberg",
    toppings: ['tomato',"onion","cheese"]
}
    function setName() {
        let codeName = document.getElementById("InputName").value;
        codeName = codeName.toUpperCase() + "'s Burger";
        Burger.name = codeName;
        renderBurger(Burger);        
    }

    function choosePatty(patty) {
        Burger.patty=patty;
        renderBurger(Burger);
    }

    function findVegetarianStatus(patty) {
        let veggieStatus = ""
        if (patty == "Veggie"){
            veggieStatus = "yes";
        }else {
            veggieStatus = "no";
        }
        document.getElementById("IsVegetarian").innerText = veggieStatus
    }
    function getPattyPrice(patty) {
        let pattyPrice; 
        switch (patty) {
            case "Beef":    pattyPrice = 200;
                            break;
            case "Veggie":  pattyPrice = 120;

                            break; 
            case "Chicken": pattyPrice = 120;
                            break; 
            case "Pulled Pork":  pattyPrice = 230;               
                            break; 
            default:        pattyPrice = 200;  
        }
        return pattyPrice;
    }



    function renderBurger(burgerToRender) {
        findVegetarianStatus(burgerToRender.patty);
        document.getElementById("CodeName").innerText=burgerToRender.name;
        let pattyPrice = getPattyPrice(burgerToRender.patty);
        document.getElementById("PriceField").innerText = Math.round((pattyPrice + basePrice) /100).toFixed(2) + "€";                
        document.getElementById("PattyField").innerText = burgerToRender.patty + " Patty";
        document.getElementById("SaladField").innerText = burgerToRender.salad + " Salad";
        document.getElementById("ToppingsField").innerText = burgerToRender.toppings.join(' and ');
        console.log('render Burger');
        console.log(burgerToRender);
       
    }

renderBurger(Burger);
let Patties = [
    {
        name: "Beef",
        pattyPrice: "200",
        isVegetarian: true
    }, {
        name: "Veggie",
        pattyPrice: "120",
        isVegetarian: true
    },
    {
        name: "Chicken",
        pattyPrice: "120",
        isVegetarian: false
    },
    {
        name: "Pulled Pork",
        pattyPrice: "230",
        isVegetarian: false
    }
]

Patties.forEach((patty,index) => {
    console.log("index "+index);
    console.log(Patties[index]);
    document.getElementById("PattyButtons").innerHTML += '<button onClick="choosePatty(\''+patty.name+'\')" id="pattySelector">'+patty.name+'</button>';
});