document.getElementById("PattyField").innerText = ".. place order ..";
let basePrice = 150 ;

    function setName() {
        let codeName = document.getElementById("InputName").value;
        codeName = codeName.toUpperCase() + "'s";
        document.getElementById("CodeName").innerText=codeName;
    }

    function choosePatty(patty) {
        let pattyPrice = getPattyPrice(patty);
        document.getElementById("PriceField").innerText = Math.round((pattyPrice + basePrice) /100).toFixed(2) + "€";                
        document.getElementById("PattyField").innerText = patty + " Patty";
        findVegetarianStatus(patty);
    }

    function findVegetarianStatus(patty) {
        let veggieStatus = ""
        if (patty == "Veggie"){
            veggieStatus = "yes";
        }else {
            veggieStatus = "no";
        }
        document.getElementById("IsVegetarian").innerText = veggieStatus
    }
    function getPattyPrice(patty) {
        let pattyPrice; 
        switch (patty) {
            case "Beef":    pattyPrice = 200;
                            break;
            case "Veggie":  pattyPrice = 120;

                            break; 
            case "Chicken": pattyPrice = 120;
                            break; 
            case "Pulled Pork":  pattyPrice = 230;               
                            break; 
            default:        pattyPrice = 200;  
        }
        return pattyPrice;
    }